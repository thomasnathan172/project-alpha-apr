from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task


@login_required
def create_task_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect(
                "projects:list_projects"
            )  # Adjust the name based on your project's URL configuration
    else:
        form = TaskForm()

    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/show_my_tasks.html", {"tasks": tasks})


# Create your views here.
