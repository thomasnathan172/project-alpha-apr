from django.urls import path
from tasks.views import create_task_view
from tasks.views import show_my_tasks

app_name = "tasks"

urlpatterns = [
    path("create/", create_task_view, name="create_task_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
