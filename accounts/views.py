from django.shortcuts import render
from django.contrib.auth import authenticate
from .forms import LoginForm
from django.contrib.auth import login
from django.shortcuts import redirect
from django.contrib.auth import logout
from .forms import SignupForm
from django.contrib.auth.models import User


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                return redirect("projects:list_projects")

    else:
        form = LoginForm()

    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("accounts:login")


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username, password=password
                )
                login(request, user)
                return redirect("projects:list_projects")
            else:
                form.add_error(
                    "password_confirmation", "The passwords do not match"
                )
    else:
        form = SignupForm()

    return render(request, "accounts/signup.html", {"form": form})


# Create your views here.
