from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from .forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    return render(
        request, "projects/list_projects.html", {"projects": projects}
    )


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(request, "projects/show_project.html", {"project": project})


@login_required
def create_view(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("projects:list_projects")
    else:
        form = ProjectForm()

    return render(request, "projects/create_project.html", {"form": form})
