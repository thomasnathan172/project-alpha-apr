from django.urls import path, include
from projects.views import list_projects
from projects.views import show_project
from projects.views import create_view
from tasks.views import show_my_tasks


app_name = "projects"

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_view, name="create_project"),
    path("tasks/", include("tasks.urls")),
]
